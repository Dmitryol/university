package com.dmitryol;
/**
 * POJO model to table exams.
 * @author Dmitry
 *
 */

public class Exam {
	
	private int id;
	private String subject;
	private String date;
	
	public Exam(int id, String subject, String date){
		this.setId(id);
		this.setSubject(subject);
		this.setDate(date);
	}
	public Exam(){};

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getSubject() {
		return subject;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}

}
