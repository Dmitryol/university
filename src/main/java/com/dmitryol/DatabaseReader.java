package com.dmitryol;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * This class reads and writes data in the database.
 * @author Dmitry
 */
public class DatabaseReader {
	/**
	 * The ArrayList of the students.	
	 */
	private ArrayList<Student> students = new ArrayList<Student>();
	/**
	 * The ArrayList of the groups.
	 */
	private ArrayList<Group> groups = new ArrayList<Group>();
	/**
	 * The ArrayList of the exams.
	 */
	private ArrayList<Exam> exams = new ArrayList<Exam>();
	
	public DatabaseReader(){
	
	}
	/**
	 * The method gets ArrayList of the students.
	 * @return ArrayList - Array of the students.
	 */
	public ArrayList<Student> getStudets(){
		students.clear();
		studentUpdate();
		return students;
		
	}
	/**
	 * The method gets ArrayList of the groups.
	 * @return ArrayList - Array of the groups. 
	 */
	public ArrayList<Group> getGroups(){
		groups.clear();
		groupsUpdate();
		return groups;		
	}
	/**
	 * The method gets ArrayList of the exams.
	 * @return ArrayList - Array of the exams.
	 */
	public ArrayList<Exam> getExams(){
		exams.clear();
		examsUpdate();
		return exams;
	}
	/**
	 * The method takes data about students and puts it in the ArrayList.
	 */
  private void studentUpdate(){
	  
	  try { 
		    Connection con=Main.pool.getConnection();
			Statement st=con.createStatement();
			st.executeQuery("select * from students");
			ResultSet res = st.getResultSet();
		
				while(res.next()){
					int id=Integer.parseInt(res.getString(1));
					int groupId=Integer.parseInt(res.getString(4));
					students.add(new Student(id, res.getString(2), res.getString(3), getTitleGroup(groupId)));					
				}
				res.close();
				con.close();
				st.close();
				
				} catch (SQLException e) {
					e.printStackTrace();
				}
			
		}
  /**
   * The method gets title of the group to the ID
   * @param id - Id group.
   * @return Title - Name of the group.
   */
  private String getTitleGroup(int id){
	  
	  String title="";  
	  try { 
		    Connection con=Main.pool.getConnection();
			Statement st=con.createStatement();
			st.executeQuery("select title from groups where id="+id);
			
			ResultSet res = st.getResultSet();
				while(res.next()){
					title=res.getString(1);					
				}
				res.close();
				con.close();
				st.close();
						
				} catch (SQLException e) {
					e.printStackTrace();
				}
	  return title;
	  
  }
  /**
   * The method gets id of the group to the title. 
   * @param title - the title of the group.
   * @return ID group. 
   */
  public int getIdGroup(String title){
	  
	  int id=0;
	  try { 
		    Connection con=Main.pool.getConnection();
			Statement st=con.createStatement();
			st.executeQuery("select id from groups where title='"+title+"'");
			
			ResultSet res = st.getResultSet();
				while(res.next()){
					id=Integer.parseInt(res.getString(1));					
				}
				res.close();
				con.close();
				st.close();
						
				} catch (SQLException e) {
					e.printStackTrace();
				}
	  return id;
	  
  }
  /**
  * The method takes data about groups and puts it in the ArrayList.
  */
  private void groupsUpdate(){
           
	  try { 
		    Connection con=Main.pool.getConnection();
			Statement st=con.createStatement();
			st.executeQuery("select * from groups");
			
			ResultSet res = st.getResultSet();
				while(res.next()){
					int id=Integer.parseInt(res.getString(1));
					groups.add(new Group(id, res.getString(2)));					
				}
				res.close();
				con.close();
				st.close();		
				} catch (SQLException e) {
					e.printStackTrace();
				}
  }
  /**
  * The method takes data about exams and puts it in the ArrayList.
  */
  private void examsUpdate(){
  	  try {
  		  Connection con=Main.pool.getConnection();
			Statement st=con.createStatement();
			st.executeQuery("select * from exams");
			
			ResultSet res = st.getResultSet();
				while(res.next()){
					int id=Integer.parseInt(res.getString(1));
					exams.add(new Exam(id, res.getString(2), res.getString(3)));					
				}
				res.close();
				con.close();
				st.close();
				
				} catch (SQLException e) {
					e.printStackTrace();
				}
  }
  /**
   * Method adds data in the database.
   * @param sql - SQL query for adding data; 
   */
  private void insertData(String sql){
	  
	  try { 
		    Connection con=Main.pool.getConnection();
			Statement st=con.createStatement();
			st.execute(sql);
			st.getConnection().close();
			st.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
	  
}
  /**
   * Adding student in the database.	  
   * @param name - The name of the student.
   * @param surname - The surname of the student.
   * @param group - The group for the student.
   */
  public void addStudent(String name, String surname, int group){
	  
	  insertData("insert  into students (name, surname, number_group) values "+"('"+name+"','"+surname+"','"+group+"');");
	 
}
  /**
   * Adding group in the database.
   * @param title - The title of the group.
   */
  public void addGroup(String title){
	  insertData("insert into groups (title) values ('"+title+"');");
  }
  /**
   * Adding exam in the database.
   * @param subject - The title of the exam.
   * @param date - The date of the exam.
   */
  public void addExam(String subject, String date){
	  insertData("insert into exams (subject, date) values ('"+subject+"','"+date+"');");
  }
}
