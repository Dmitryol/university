package com.dmitryol;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
/**
 * Class controller. 
 * @author Dmitry
 */
public class Controller {
	  
	  private ObservableList<Student> studData = FXCollections.observableArrayList();
	  private ObservableList<Group> groupData = FXCollections.observableArrayList();
	  private ObservableList<Exam> examData = FXCollections.observableArrayList();
	  private DatabaseReader database= new DatabaseReader();
	  
	  @FXML
	  TextField firstName, secondName, groupTitle, examTitle;
	  @FXML
	  ComboBox<String> group, year, month, day;
	  @FXML
	  Button addStudent, addGroup, addExam;
	  @FXML
	  // Table students.
	  TableView<Student> students;
	  @FXML
	  TableColumn<Student, Integer> studId; 
	  @FXML
	  TableColumn<Student, String> studName; 
	  @FXML
	  TableColumn<Student, String> studSurname; 
	  @FXML
	  TableColumn<Student, String> number_group;
	  // Table groups.
	  @FXML
	  TableView<Group> groups;
	  @FXML
	  TableColumn<Group, Integer> groupId; 
	  @FXML
	  TableColumn<Group, String> groupName; 
	  // Table exams.
	  @FXML
	  TableView<Exam> exams;
	  @FXML
	  TableColumn<Exam, Integer> examId; 
	  @FXML
	  TableColumn<Exam, String> examName; 
	  @FXML
	  TableColumn<Exam, String> examDate; 
	  /**
	   * The method uses for initialize data in javaFX.
	   */
	   @FXML
	   private void initialize(){
	    	initStud();
	    	// Filling group by data.
	    	group.getItems().addAll(getGroups());
	    	initGroup();
	        initExam();
	        year.getItems().addAll(getYear());
	        month.getItems().addAll(getMonth());
	        day.getItems().addAll(getDay());
	    	// Setting each column table students in according POJO student.
	    	studId.setCellValueFactory(new PropertyValueFactory<Student, Integer>("id"));
	    	studName.setCellValueFactory(new PropertyValueFactory<Student, String>("name"));
	    	studSurname.setCellValueFactory(new PropertyValueFactory<Student, String>("surname"));
	    	number_group.setCellValueFactory(new PropertyValueFactory<Student, String>("group"));
	    	// Table groups.
	    	groupId.setCellValueFactory(new PropertyValueFactory<Group, Integer>("id"));
	    	groupName.setCellValueFactory(new PropertyValueFactory<Group, String>("title"));
	    	// Table exams.
	    	examId.setCellValueFactory(new PropertyValueFactory<Exam, Integer>("id"));
	    	examName.setCellValueFactory(new PropertyValueFactory<Exam, String>("subject"));
	    	examDate.setCellValueFactory(new PropertyValueFactory<Exam, String>("date"));
	    	// Setting data source for tables.
	    	students.setItems(studData);
	    	groups.setItems(groupData);
	        exams.setItems(examData);
	    	
	    	
	    }
	   /**
	    * Filling data ObservableList for table students.
	    */
	  @FXML
	   private void initStud(){
		  // Clearing data before initialization.
		  studData.clear();
		   // Amount students in database.
	       int a=database.getStudets().size();
		   for(int i=0;i<a;i++){
			   studData.add(database.getStudets().get(i));
			   
	    }
	   }
	  /**
	   * Getting list groups for students.
	   * @return String[] of the groups.
	   */
	  @FXML
	  private String[] getGroups(){
		  // Amount groups in database.
		  int size=database.getGroups().size();
		  String [] groups= new String [size];
		  for(int i=0; i<size; i++){
			  groups[i]=database.getGroups().get(i).getTitle();
		  }
		  return groups;
	  }
	  /**
	   * Adding student into database. 
	   */
	  @FXML
	  public void addStudent(){
		  database.addStudent(firstName.getText(), secondName.getText(), database.getIdGroup(group.getValue()));
		  // Update data in the table.
		  initStud();
		  // Reset TextFields.
		  firstName.setText("");
		  secondName.setText("");
		  
	  }
	  /**
	    * Filling data ObservableList for table groups.
	    */
	  @FXML
	  private void initGroup(){  
		  groupData.clear();
		  int size=database.getGroups().size();
		  for(int i=0;i<size;i++){
			  groupData.add(database.getGroups().get(i));
		  }
		  
	  }
	  /**
	   * Adding group of the students into database. 
	   */
	  @FXML
	  public void addGroup(){
		  database.addGroup(groupTitle.getText());
		  // Update data in the table.
		  initGroup();
		  groupTitle.setText("");
		  // Update ComboBox group.
		  group.getItems().addAll(getGroups());
	  }
	  /**
	    * Filling data ObservableList for table exams.
	    */
	  @FXML
	  private void initExam(){
		  examData.clear();
		  int size=database.getExams().size();
		  for(int i=0;i<size;i++){
			  examData.add(database.getExams().get(i));
		  }
	  }
	  /**
	   * Adding exam into database.
	   */
	  @FXML
	  public void addExam(){
		  database.addExam(examTitle.getText(), getDate());
		  // Update data in the table.
		  initExam();
		  examTitle.setText("");
	  }
	  /**
	   * Method creates list years for ComboBox. 
	   * @return The array years.
	   */
	  private String[] getYear(){
			String[] years = new String[24];
		    int year = 2017;
			for(int i=0; i<years.length; i++){
				years[i]= ""+year+"";
				year++;
			}
			return years;
		}
	  /**
	   * Method creates list mouths for ComboBox. 
	   * @return The array months.
	   */
		private String[] getMonth(){
			String[] months = new String[12];
		    int month =1;
			for(int i=0; i<months.length; i++){
				months[i]= ""+month+"";
				month++;
			}
			return months;
		}
		/**
		   * Method creates list days for ComboBox. 
		   * @return The array days.
		   */
		private String[] getDay(){
			String[] days = new String[31];
		    int day =1;
			for(int i=0; i<days.length; i++){
				days[i]= ""+day+"";
				day++;
			}
			return days;
		}
		/**
		 * Setting year, month,day and converting them to the format date. 
		 * @return The date in the format "year-month-day"
		 */
		private String getDate(){
			return year.getValue()+"-"+month.getValue()+"-"+day.getValue();
		}

}
