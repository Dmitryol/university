package com.dmitryol;
/**
 * POJo model to table groups.
 * @author Dmitry
 *
 */

public class Group {
	
	private int id;
	private String title;
	
	public Group(int id, String title){
		
		this.setId(id);
		this.setTitle(title);
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

}
