create table exams(
id int not null default nextval('exam_increment') primary key,
subject varchar(30) not null,
date date not null);